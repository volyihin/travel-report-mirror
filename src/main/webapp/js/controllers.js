/**
 * Created by nataliyamakarova on 15.10.14.
 */

var app = angular.module('getbookmarks');

app.controller('StoryListController', ['$scope', '$rootScope', 'Auth', 'Trip', 'Story', '$location', '$stateParams',
    function ($scope, $rootScope, Auth, Trip, Story, $location, $stateParams) {

        $scope.trip = Trip.get({tripId: $stateParams.tripId});

        var currentUser = Auth.user;

        angular.element('.navbar-default .navbar-nav > li > a ').css("color", "black");
        angular.element('.navbar-default .navbar-brand').css("color", "black");


        $scope.stories = Story.query({tripId: $stateParams.tripId});

        $scope.delete = function (index) {
            console.log("Delete story");
            $scope.story = $scope.stories[index];
            $scope.stories.splice(index, 1);

            $scope.story.$delete(function (story, headers) {
                toastr.success("История удалена");
            });
        };

        $scope.selectTripImage = function (story) {
                var storyUrl = story.image.url;
                $scope.trip.image.url = storyUrl;

                $scope.trip.$save(function (success) {
                    toastr.success("Выбрана обложка путешествия");
                }, function (error) {
                    toastr.error("Ошибка выбора обложки путешествия");
                })

        };

        $scope.create = function () {
            console.log("Create story");
            $scope.story = new Story();
            $scope.story.tripId = $scope.trip.id;
            $scope.story.title = moment().format("dddd, MMMM Do YYYY");
            $scope.story.text = 'Описание';
            $scope.story.$save(function (response) {
                console.info($scope.story.id);
                $location.path("/trips/" + $scope.tripId + "/stories/" + $scope.story.id);
            });
        }
    }]);

app.controller('StoryCreateController', ['$scope', '$rootScope', '$location','Auth', 'Story', 'Record', 'Resize', '$stateParams',
    function ($scope, $rootScope, $location,Auth, Story, Record, Resize, $stateParams) {

        var storyId = $stateParams.storyId;

        if (storyId === undefined) {
            $location.path('/stories')
        }

        $scope.story = Story.get({storyId: storyId},
                 function (success) {
                    console.info(success);
                },
                function (error) {
                    console.error(error);
                });

        $scope.story.records = Record.query({storyId: storyId});

        $scope.story.$promise.then(function (result) {
            if ($scope.story.image && $scope.story.image.url) {
                angular.element('.navbar-default .navbar-nav > li > a ').css("color", "white");
                angular.element('.navbar-default .navbar-brand').css("color", "white");
                angular.element('.navbar-right .btn-default').css("color", "white");
            } else {
                angular.element('.navbar-default .navbar-nav > li > a ').css("color", "black");
                angular.element('.navbar-default .navbar-brand').css("color", "black");
                angular.element('.navbar-right .btn-default').css("color", "black");
            }

        });


        $scope.file_changed = function (element) {

            Resize.resizeAndPost(
                element.files,
                $scope.story.id,
                function (record) {
                    $scope.story.records.push(record);
                },
                function (error) {
                    console.error(error);
                }
            );
        }

        $scope.selectStoryImage = function (record) {
            console.log("selectStoryImage");
            $scope.story.image = record.image;
            $scope.story.$updateImage({imageUrl: record.image.url}, function (response) {
                toastr.success("Выбрана обложка истории");
                $scope.$apply();
                console.info($scope.story.id);
            });


        };

        $scope.addTextRecord = function () {
            console.log("Add record");
            var newRecord = new Record();
            newRecord.type = 'Text';
            newRecord.storyId = storyId;

            newRecord.$saveText({storyId: $scope.story.id},
            function (record) {
                $scope.story.records.push(response);
            },
            function (response) {
                console.error(response);
            });
        };

        $scope.addTextRecordToImage = function (index) {

            var newRecord = new Record();
            newRecord.type = 'Text';
            newRecord.storyId = $scope.story.id;
            $scope.story.user = Auth.user;

            newRecord.$saveText({storyId: $scope.story.id, index : index + 1},
            function (record) {
                $scope.story.records.splice(index + 1, 0, record);

            },
            function (response) {
                console.error(response);
            });

        };


        $scope.addRecord = function () {
            console.log("Add record");

            console.log($scope.newRecord);

            $scope.newRecord.$save({storyId: $scope.story.id}, function (record, headers) {
                toastr.success("Создана новая запись");
            }).then(function (response) {
                $scope.story.records.push(response);

            });

            $scope.newRecord = new Record();

        };

        $scope.updateStoryText = function (story, data) {
            console.log("update story");
            console.log(story)

            $scope.story.text = data;
            $scope.story.$save(function (response) {
                toastr.success("История сохранена");
                console.info($scope.story.id);
            }, function (responce) {
                toastr.error("Не удалось обновить историю");
            });
        };

        $scope.updateStory = function (story) {
            console.log("update story");
            console.log(story)

            $scope.story.$save(function (response) {
                toastr.success("История сохранена");
                console.info($scope.story.id);
            });
            $scope.isCollapsed = true;

        };

        $scope.updateRecord = function (record) {
            console.log("update record storyId" + $scope.story.id);
            console.log("update record recordId" + record.id);
            console.log("update record comment" + record.text);
            var updateRecord = new Record();
            updateRecord.id = record.id;
            updateRecord.storyId = $scope.story.id;
            updateRecord.type = "Text";
//            updateRecord.image = record.image;
            updateRecord.text = record.text;
            updateRecord.$update({ storyId: $scope.story.id, recordId: record.id}, function (response) {
                toastr.success("Обновили комментарий");
            });

        };

        $scope.add = function () {
            console.log("Add item");

            console.log($scope.newRecord);

            $scope.newRecord.$save({storyId: $scope.story.id}, function (record, headers) {
                toastr.success("Создана новая запись");
            }).then(function (response) {
                $scope.story.records.push(response);

            });

            $scope.newRecord = new Record();

        }

        $scope.delete = function (index) {
            $scope.record = $scope.story.records[index];

            $scope.story.records.splice(index, 1);

            if ($scope.record === null) {
                console.log("Delete null record ");
                return;
            }

            console.log("Delete record " + $scope.story.id + " " + $scope.record.id);

            var deletedRecord = new Record();

            deletedRecord.$delete({storyId: $scope.story.id, recordId: $scope.record.id},
            function (record) {
                toastr.success("Запись удалена");
            },
            function (response) {
                console.error(response);
            });
        }

        $scope.save = function () {
            $scope.story.$save(function (story, headers) {
                toastr.success("Создана новая история");
                $location.path("/" + $rootScope.username + "/");
            });
        };

        $scope.rotate = function (angle) {
            $scope.angle = angle;
        };

        var move = function (origin, destination) {
            console.info("origin=" + origin + " destination=" + destination);
            Record.move({storyId: $scope.story.id,origin :origin, destination:destination},
            function(response) {
                console.info(response);
                var temp = $scope.story.records[destination];
                $scope.story.records[destination] = $scope.story.records[origin];
                $scope.story.records[origin] = temp;
                toastr.success("Двинули");
            },
            function(error) {
                console.error(error);
            });
        };

        $scope.moveUp = function(index){
            move(index, index - 1);
        };

        $scope.moveDown = function(index){
            move(index, index + 1);
        };

    }]);

app.controller('LoginCtrl',
    ['$rootScope', '$scope', '$location', '$window', 'Auth', function ($rootScope, $scope, $location, $window, Auth) {

        $scope.rememberme = true;

        $scope.login = function () {
            Auth.login({
                    username: $scope.username,
                    password: $scope.password,
                    rememberme: $scope.rememberme
                },
                function (response) {
                    console.trace("login successfully")
                    $location.path('/trips');
                },
                function (error) {
                    $rootScope.error = "Failed to login";
                });
        };

        $scope.loginOauth = function (provider) {
            $window.location.href = '/auth/' + provider;
        };
    }]);


app.controller('NavCtrl', ['$rootScope', '$scope', '$location', 'Auth', '$state',
    function ($rootScope, $scope, $location, Auth, $state) {
        $scope.user = Auth.user;
        $scope.userRoles = Auth.userRoles;
        $scope.accessLevels = Auth.accessLevels;

        $scope.logout = function () {
            Auth.logout(function () {
                $location.path('#/home');
//        $state.go('public.home');
            }, function () {
                $rootScope.error = "Failed to logout";
            });
        };
    }]);

app.controller('RegisterCtrl',
    ['$rootScope', '$scope', '$location', 'Auth', function ($rootScope, $scope, $location, Auth) {
        $scope.role = Auth.userRoles.user;
        $scope.userRoles = Auth.userRoles;

        $scope.register = function () {
            Auth.register({
                    username: $scope.username,
                    password: $scope.password,
                    role: $scope.role
                },
                function () {
                    $location.path('/');
                },
                function (err) {
                    $rootScope.error = err;
                });
        };
    }]);
//
//function BlogController($scope, $routeParams, User) {
//    console.log($routeParams.user);
//    $scope.user = User.get($routeParams.user);
//    console.log($scope.user);
//    $scope.username = $routeParams.user;
//
//
//}

app.controller('TripController', ['$scope', '$http', '$location', 'fgDelegate', 'Trip', 'Auth', 'Image', function ($scope, $http, $location, fgDelegate, Trip, Auth, Image) {

    $scope.watchFlow = function () {
        // make sure ngRepeat is finished rendering
        $scope.$watch('$last', function () {
            fgDelegate.getFlow('demoGird').itemsChanged();
        });
    };

    var username = Auth.user.username;
    console.log('get trips by username = ' + username);
    $scope.items = Trip.query({tripId: '', username: username}, function (response) {
        var flow = fgDelegate.getFlow('demoGird');
        flow.minItemWidth = 300;
//        flow.refill(true);
//
        $scope.watchFlow();
    });

    $scope.stories = function (id) {
        $location.path('/trips/' + id + '/stories');
    }

    $scope.addItem = function () {

        var getRandomColor = function () {
            var letters = '0123456789ABCDEF'.split('');
//        var color = '#';
            var color = '';
            for (var i = 0; i < 6; i++) {
                color += letters[Math.floor(Math.random() * 16)];
            }
            return color;
        };

        var newImage = new Image();

        var url = 'http://placehold.it/400x300/' + getRandomColor() + '/fff&text=TRIP+#' + $scope.items.length;

        newImage.url = url;
        newImage.$save(function (response) {

            newImage = response;
            var newItem = new Trip();
            newItem.image = newImage;
            newItem.username = Auth.user.username;

            newItem.$save(function (response) {
                console.log(response);
                // add a new item;

            }, function (error) {
                console.error(error)
            })

            $scope.items.splice(0, 0, newItem);
            $scope.watchFlow();

        }, function (error) {
            console.error(error)
        });


    }

    $scope.updateTrip = function (trip) {
        trip.$save(function (response) {
            console.log(response);
        }, function (error) {
            console.error(error)
        })
    }

    $scope.deleteItem = function (index) {
        console.log("delete item");
        console.log(index)

        var item = $scope.items[index];
        $scope.items.splice(index, 1);

        item.$delete(function (item) {
            toastr.success("История удалена");
        }, function (data) {
            console.error(data);
//                toastr.error("История удалена");
        });

        $scope.watchFlow();

    }

    $scope.changeWidth = function (width) {
        console.log("change width = " + width);
        var flow = fgDelegate.getFlow('demoGird')

        flow.minItemWidth += width;
        $scope.watchFlow();
//        flow.refill(true);
    };

    $scope.watchFlow = function () {
        // make sure ngRepeat is finished rendering
        $scope.$watch('$last', function () {
            fgDelegate.getFlow('demoGird').itemsChanged();
        });
    };

    // then you can:
    // homePageGrid.minItemWidth = 150;
    // homePageGrid.refill();

}]);

app.controller('AdminController', ['$scope', 'User', function ($scope, User) {
    $scope.users = User.query();

}]);






