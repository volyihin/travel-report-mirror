/**
 * Created by nataliyamakarova on 15.10.14.
 */


var services = angular.module("services", ["ngResource"])

services.factory('Story', function ($resource) {
    var Story = $resource('/api/v1/stories/:storyId', {storyId: '@id'},
        {
            'update': { method: 'PUT' },
            'updateImage': {url:'/api/v1/stories/image/:storyId', method: 'POST', params: {storyId: '@id'}}
//            'query': {method: 'GET', params: {userId: 'userId'}, isArray: true}
        });
    Story.prototype.isNew = function () {
        return (typeof(this.id) === 'undefined');
    }
    return Story;
});

services.factory('Record', function ($resource) {
    var Record = $resource('/api/v1/stories/:storyId/records/:recordId', {storyId: 'storyId', recordId: '@id'}, {
        'update': { method: 'PUT', params: {recordId: '@id'}},
        'move': {url:'/api/v1/stories/:storyId/records/move', method: 'GET', params: {storyId: 'storyId',origin : 'origin', destination:'destination'}},
        'saveText': {url:'/api/v1/stories/:storyId/records/text/:recordId', method: 'POST', params: {recordId: '@id'}}

    });
    Record.prototype.isNew = function () {
        return (typeof(this.id) === 'undefined');
    }
    return Record;
});

services.factory('Trip', function ($resource) {
    var Trip = $resource('/api/v1/trips/:tripId', {tripId: '@id'}, {
        'update': { method: 'PUT', params: {tripId: '@id'}}
    });
    Trip.prototype.isNew = function () {
        return (typeof(this.id) === 'undefined');
    }
    return Trip;
});

services.factory('Image', function ($resource) {
    var Image = $resource('/api/v1/images');
    Image.prototype.isNew = function () {
        return (typeof(this.id) === 'undefined');
    }
    return Image;
});

services.factory('Auth', function ($http, $window) {

    var accessLevels = routingConfig.accessLevels
        , userRoles = routingConfig.userRoles
        , currentUser = JSON.parse($window.sessionStorage.getItem('user')) || { username: '', role: userRoles.public };

//    $window.sessionStorage.removeItem('user');

    function changeUser(user) {
        angular.extend(currentUser, user);
    }

    return {
        authorize: function (accessLevel) {
            var role = currentUser.role;

            if (role === undefined) {
                console.error("Role must not be undefined");
                return false;
            }

            if (role === null) {
                console.error("Role must not be null");
            }

            return accessLevel.bitMask & role.bitMask;
        },
        isLoggedIn: function (user) {
            if (user === undefined) {
                user = currentUser;
            }
            return user.role.title === userRoles.user.title || user.role.title === userRoles.admin.title;
        },
        register: function (user, success, error) {
            $http.post('/api/v1/users/register', user).success(function (res) {
                changeUser(res);
                $window.sessionStorage.setItem('user', JSON.stringify(user));
                success(user);
            }).error(error);
        },
        login: function (user, success, error) {

            $http.post('/api/v1/users/login', user).success(function (user) {
                console.log(user);
                changeUser(user);
                $window.sessionStorage.setItem('user', JSON.stringify(user));
                success(user);
            }).error(error);
        },
        logout: function (success, error) {
            $http.post('/api/v1/users/logout', currentUser).success(function () {
                changeUser({
                    username: '',
                    role: userRoles.public
                });
                $window.sessionStorage.removeItem('user');
                success();
            }).error(error);
        },
        setDebugLogin: function () {
            var debugUser = {};
            debugUser.username = "user";
            this.login(debugUser, function(user){

            });
        },
        accessLevels: accessLevels,
        userRoles: userRoles,
        user: currentUser
    };
});

services.factory('User', function ($resource) {
    var User = $resource('/api/v1/users/:username', {username: '@username'});
    User.prototype.isNew = function () {
        return (typeof(this.id) === 'undefined');
    }
    return User;
});

services.factory('Users', function ($http) {
    return {
        getAll: function (success, error) {
            $http.get('/users').success(success).error(error);
        }
    };
});


services.service('Resize', ['Auth', '$http', function (Auth, $http) {

    this.fileSize = function (size) {
        var i = Math.floor(Math.log(size) / Math.log(1024));
        return (size / Math.pow(1024, i)).toFixed(2) * 1 + ' ' + ['B', 'kB', 'MB', 'GB', 'TB'][i];
    };

    this.init = function (outputQuality) {
        this.outputQuality = (outputQuality === 'undefined' ? 0.8 : outputQuality);
    }

    this.resizeAndPost = function (files, storyId, success, error) {

        this.init();

        for (var i in files) {

            if (typeof files[i] !== 'object') return false;

            this.photo(files[i], 1000, 'file', function (file) {
                console.log('Image file: ', file);

                var fd = new FormData();
                fd.append('file', file);
                fd.append('username', Auth.user.username);
                fd.append('storyId', storyId);
                $http.post('/api/v1/upload/image', fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined}
                })
                    .success(function (data, status, headers, config) {
                        success(data);
                        // this callback will be called asynchronously
                        // when the response is available
                    })
                    .error(function (data, status, headers, config) {
                        error();
                    });
            });
        }
    }

    this.photo = function (file, maxSize, outputType, callback) {

        var _this = this;

        var reader = new FileReader();
        reader.onload = function (readerEvent) {
            _this.resize(readerEvent.target.result, maxSize, outputType, callback);
        }
        reader.readAsDataURL(file);

    }

    this.resize = function (dataURL, maxSize, outputType, callback) {

        var _this = this;

        var image = new Image();
        image.onload = function (imageEvent) {

            // Resize image
            var canvas = document.createElement('canvas'),
                width = image.width,
                height = image.height;
            if (width > height) {
                if (width > maxSize) {
                    height *= maxSize / width;
                    width = maxSize;
                }
            } else {
                if (height > maxSize) {
                    width *= maxSize / height;
                    height = maxSize;
                }
            }
            canvas.width = width;
            canvas.height = height;
            canvas.getContext('2d').drawImage(image, 0, 0, width, height);

            _this.output(canvas, outputType, callback);

        }
        image.src = dataURL;

    }

    this.output = function (canvas, outputType, callback) {

        switch (outputType) {

            case 'file':
                canvas.toBlob(function (blob) {
                    callback(blob);
                }, 'image/jpeg', 0.8);
                break;

            case 'dataURL':
                callback(canvas.toDataURL('image/jpeg', 0.8));
                break;

        }

    }
}]);