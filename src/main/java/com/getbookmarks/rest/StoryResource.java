package com.getbookmarks.rest;

import java.util.List;

import com.getbookmarks.domain.record.Image;
import com.getbookmarks.repository.ImageRepository;
import com.getbookmarks.repository.RecordRepository;
import com.getbookmarks.rest.exception.StoryNotFoundException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.getbookmarks.domain.Story;
import com.getbookmarks.domain.Trip;
import com.getbookmarks.repository.StoryRepository;

@Controller
@RequestMapping("/stories")
public class StoryResource {

    public static final Logger logger = Logger.getLogger(StoryResource.class);

    private RecordRepository recordRepository;
    private StoryRepository storyRepository;
    private ImageRepository imageRepository;

    @Autowired
    public StoryResource(RecordRepository recordRepository, StoryRepository storyRepository,ImageRepository imageRepository) {
        this.recordRepository = recordRepository;
        this.storyRepository = storyRepository;
        this.imageRepository = imageRepository;
    }

    @RequestMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Story> submitStory(@RequestBody Story story) {
        storyRepository.save(story);
        return new ResponseEntity<Story>(story,HttpStatus.OK);
    }

    @RequestMapping(value = "/{storyId}", method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Void> updateStory(@RequestBody Story story) {
        logger.info("post story " +  story);
        storyRepository.save(story);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(value = "/image/{storyId}", method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Void> updateImageStory(@PathVariable("storyId") String storyId, @RequestParam("imageUrl") String imageUrl) {
        logger.info("update Image Story " +  storyId);
        Story story = storyRepository.findOne(storyId);
        Image image = imageRepository.findByUrl(imageUrl);
        story.setImage(image);
        storyRepository.save(story);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Story> tripStories(@RequestParam("tripId") String tripId) {
        logger.info("get stories by tripId =  " +  tripId);
        List<Story> storyList = storyRepository.findByTripId(tripId);
        logger.info("get " + storyList.size() + " stories");
        return storyList;
    }

    @RequestMapping(value = "/{storyId}",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Story showStory(@PathVariable("storyId") String storyId) {
        logger.info("get story " +  storyId);
        Story story = storyRepository.findOne(storyId);
        if (story == null) {
            throw new StoryNotFoundException(storyId);
        }

        logger.info("story " +  story);
        return story;
    }

    @RequestMapping(value = "/{storyId}",method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<Void> deleteStory(@PathVariable("storyId") String storyId) {
        logger.info("delete story " +  storyId);
        storyRepository.delete(storyId);
        ResponseEntity<Void> responseEntity = new ResponseEntity<Void>(HttpStatus.OK);
        return responseEntity;
    }

}
