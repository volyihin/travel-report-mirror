package com.getbookmarks.rest;

import com.getbookmarks.domain.Story;
import com.getbookmarks.domain.Trip;
import com.getbookmarks.repository.ImageRepository;
import com.getbookmarks.repository.TripRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by nataliyamakarova on 12.12.14.
 */
@Controller
@RequestMapping(value = "/trips")
public class TripResource {


    public static final Logger logger = Logger.getLogger(TripResource.class);

    private TripRepository tripRepository;
    private ImageRepository imageRepository;

    @Autowired
    public TripResource(TripRepository tripRepository, ImageRepository imageRepository) {
        this.tripRepository = tripRepository;
        this.imageRepository = imageRepository;
    }

    @RequestMapping(method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Trip> createTrip(@RequestBody Trip newTrip) {
        logger.info("create trip " + newTrip);
        tripRepository.save(newTrip);
        return new ResponseEntity<Trip>(newTrip,HttpStatus.OK);
    }

    @RequestMapping(value = "/{tripId}",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Trip> updateTrip(@RequestBody Trip trip) {
        logger.info("update trip " + trip);

        imageRepository.save(trip.getImage());

        tripRepository.save(trip);
        return new ResponseEntity<Trip>(trip,HttpStatus.OK);
    }

    @RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Trip> tripsBy(@RequestParam("username") String username) {
        logger.info("get trip by user " + username);
        List<Trip> tripList = tripRepository.findByUsername(username);
        logger.info("get " + tripList.size() + " trips");
        return tripList;
    }

    @RequestMapping(value = "/{tripId}",method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<Void> deleteTrip(@PathVariable("tripId") String tripId) {
        logger.info("delete trip " + tripId);
        tripRepository.delete(tripId);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(value = "/{tripId}")
    @ResponseBody
    public ResponseEntity<Trip> getTrip(@PathVariable("tripId") String tripId) {
        logger.info("get trip " + tripId);
        Trip oneTrip = tripRepository.findOne(tripId);
        return new ResponseEntity<Trip>(oneTrip,HttpStatus.OK);
    }


//    @RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
//    @ResponseBody
//    public List<Trip> allTrips() {
//        return tripRepository.findAll();
//    }
}
