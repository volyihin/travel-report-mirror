package com.getbookmarks.rest;

import com.getbookmarks.domain.record.Image;
import com.getbookmarks.domain.record.Record;
import com.getbookmarks.repository.ImageRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * Created by nataliyamakarova on 13.12.14.
 */
@Controller
@RequestMapping(value = "/images")
public class ImageResource {

    private Logger logger = Logger.getLogger(ImageResource.class);


    private ImageRepository imageRepository;

    @Autowired
    public ImageResource(ImageRepository imageRepository) {
        this.imageRepository = imageRepository;
    }

    @RequestMapping(method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Image> createImage(@RequestBody Image image) {
        logger.info("create image");
        imageRepository.save(image);
        logger.info("create image " + image);
        return new ResponseEntity<Image>(image, HttpStatus.CREATED);
    }
}
