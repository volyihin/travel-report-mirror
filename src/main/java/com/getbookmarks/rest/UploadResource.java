package com.getbookmarks.rest;

import com.getbookmarks.domain.record.Image;
import com.getbookmarks.domain.record.ImageRecord;
import com.getbookmarks.domain.record.Record;
import com.getbookmarks.domain.Story;
import com.getbookmarks.repository.ImageRepository;
import com.getbookmarks.repository.RecordRepository;
import com.getbookmarks.repository.StoryRepository;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.FileImageInputStream;
import javax.imageio.stream.ImageInputStream;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.UUID;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import com.google.common.base.Strings;

/**
 * Created by nataliyamakarova on 11.09.14.
 */
@Controller
@RequestMapping("/upload")
public class UploadResource {

    private Lock lock = new ReentrantLock();

    @Autowired
    private Environment env;

    private static final String UPLOAD_DIR = "/img";

    public static final Logger logger = Logger.getLogger(UploadResource.class);

    private RecordRepository recordRepository;
    private StoryRepository storyRepository;
    private ImageRepository imageRepository;
    public final static String IMAGE_PATH = (Strings.isNullOrEmpty(System.getenv("OPENSHIFT_DATA_DIR")))? System.getenv("CATALINA_HOME") : System.getenv("OPENSHIFT_DATA_DIR");

    @Autowired
    public UploadResource(RecordRepository recordRepository, StoryRepository storyRepository, ImageRepository imageRepository) {
        this.recordRepository = recordRepository;
        this.storyRepository = storyRepository;
        this.imageRepository = imageRepository;
    }

    @RequestMapping(method = RequestMethod.POST, headers = "content-type=multipart/form-data")
    public ResponseEntity<ImageRecord> uploadRecordMultipart(
            final HttpServletRequest request,
            final HttpServletResponse response,
            @RequestParam("file") final MultipartFile multiPart) throws ServletException, IOException {

        String storyId = getFormValue(request, "storyId");
        String username = getFormValue(request, "username");

        logger.info("save basic image record " + " storyId=" + storyId + " username=" + username);

        File imageFile = fileFromRequest(multiPart, username);

        String originalFilename = multiPart.getOriginalFilename();
        String imageLocalUrl = UPLOAD_DIR + File.separator + username + File.separator + originalFilename;
        Image image = new Image(imageLocalUrl, isVertical(imageFile));
        imageRepository.save(image);
        Story story = storyRepository.findOne(storyId);
        ImageRecord record = new ImageRecord(image, storyId);
        recordRepository.save(record);

        story.addRecord(record);

        storyRepository.save(story);

        logger.info("saved story " + story);


        return new ResponseEntity<ImageRecord>(record, HttpStatus.OK);

    }


    private boolean isVertical(File imageFile) throws IOException {
        Dimension dimension = getImageDimension(imageFile);
        return dimension.getHeight() > dimension.getWidth();

    }


    private File fileFromRequest(MultipartFile multiPart, String username) throws IOException, ServletException {

        String uploadFilePath = IMAGE_PATH + File.separator + UPLOAD_DIR + File.separator + username;

        File fileSaveDir = new File(uploadFilePath);
        if (!fileSaveDir.exists()) {
            fileSaveDir.mkdirs();
        }
        String fileName = UUID.randomUUID().toString() + ".jpg";

        String fullpath = uploadFilePath + File.separator + fileName;
        Path imagePath = Paths.get(fullpath);
        if (!Files.exists(imagePath)) {
            imagePath = Files.createFile(imagePath);
        }
        File imageFile = imagePath.toFile();
        multiPart.transferTo(imageFile);
        logger.info("Path = " + imagePath.toUri().toString());
        return imageFile;
    }

    private String getFormValue(HttpServletRequest request, String key) {
        for (String k : request.getParameterMap().keySet()) {

            if (k.equals(key)) {

                for (String value : request.getParameterValues(key)) {
                    return value;
                }
            }
        }
        return "";
    }

    /**
     * Gets image dimensions for given file
     *
     * @param imgFile image file
     * @return dimensions of image
     * @throws IOException if the file is not a known image
     */
    public static Dimension getImageDimension(File imgFile) throws IOException {
        int pos = imgFile.getName().lastIndexOf(".");
        if (pos == -1)
            throw new IOException("No extension for file: " + imgFile.getAbsolutePath());
        String suffix = imgFile.getName().substring(pos + 1);
        Iterator<ImageReader> iter = ImageIO.getImageReadersBySuffix(suffix);
        if (iter.hasNext()) {
            ImageReader reader = iter.next();
            try {
                ImageInputStream stream = new FileImageInputStream(imgFile);
                reader.setInput(stream);
                int width = reader.getWidth(reader.getMinIndex());
                int height = reader.getHeight(reader.getMinIndex());
                return new Dimension(width, height);
            } catch (IOException e) {
                logger.warn("Error reading: " + imgFile.getAbsolutePath(), e);
            } finally {
                reader.dispose();
            }
        }

        throw new IOException("Not a known image file: " + imgFile.getAbsolutePath());
    }

    /*
        TESTING RESIZED UPLOAD --------


     */

    @RequestMapping(value = "image", method = RequestMethod.POST/*, headers = "content-type=multipart/form-data"*/)
    public ResponseEntity<ImageRecord> uploadImage(
            final HttpServletRequest request,
            final HttpServletResponse response,
            @RequestParam("file") final MultipartFile multiPart) throws ServletException, IOException {

        String storyId = getFormValue(request, "storyId");
//        String comment = getFormValue(request, "comment");
        String username = getFormValue(request, "username");


        File imageFile = fileFromRequest(multiPart, username);

        String originalFilename = imageFile.getName();
        String imageLocalUrl = UPLOAD_DIR + File.separator + username + File.separator + originalFilename;
        Image image = new Image(imageLocalUrl, isVertical(imageFile));
        imageRepository.save(image);
        try {
            lock.lock();    
            Story story = storyRepository.findOne(storyId);
            ImageRecord record = new ImageRecord(image, storyId);

            logger.info("save resized image record " + record.toString());
            recordRepository.save(record);
            story.addImageRecord(record);
            storyRepository.save(story);
            logger.info("saved story " + story);

        return new ResponseEntity<ImageRecord>(record, HttpStatus.OK);
         } finally {
            lock.unlock();

        }
    }
}