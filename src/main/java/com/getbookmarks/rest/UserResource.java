package com.getbookmarks.rest;

import com.getbookmarks.domain.User;
import com.getbookmarks.repository.UserRepository;
import com.google.common.collect.Iterables;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * Created by volyihin on 8/29/14.
 */
@Controller
@RequestMapping(value = "/users")
public class UserResource {

    public static final Logger logger = Logger.getLogger(UserResource.class);

    private UserRepository userRepository;

    @Autowired
    public UserResource(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @RequestMapping(value = "/{username}", method = RequestMethod.GET)
    public ResponseEntity<Void> exist(@PathVariable("username") String username) {
        logger.info(username);

        User currentUser = userRepository.findByUsername(username);
        if (currentUser != null) {
            return new ResponseEntity<Void>(HttpStatus.OK);
        } else {
            return new ResponseEntity<Void>(HttpStatus.UNAUTHORIZED);
        }
    }

    @RequestMapping(value = "/{username}/roles", method = RequestMethod.GET)
    public ResponseEntity<Void> getRoles(@PathVariable("username") String username) {
        logger.info(username);

        User currentUser = userRepository.findByUsername(username);
        if (currentUser != null) {
            return new ResponseEntity<Void>(HttpStatus.OK);
        } else {
            return new ResponseEntity<Void>(HttpStatus.UNAUTHORIZED);
        }
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<User[]> getUsers() {

        User[] users = Iterables.toArray(userRepository.findAll(), User.class);

        return new ResponseEntity<User[]>(users, HttpStatus.OK);

    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<User> login(@RequestBody User user) {

        String username = user.getUsername();
        logger.info("login " + username);

        User currentUser = userRepository.findByUsername(username);

        //TODO check password


        if (currentUser != null) {
            currentUser.setPassword(null);
            logger.info("login " + username + " successfully with role " + currentUser.getRole());
            return new ResponseEntity<User>(currentUser, HttpStatus.OK);
        } else {
            logger.info(username + " not login");
            return new ResponseEntity<User>(HttpStatus.UNAUTHORIZED);
        }
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity<User> register(@RequestBody User user) {

        String username = user.getUsername();
        logger.info("register " + username);

        User currentUser = userRepository.findByUsername(username);

        //TODO check password


        if (currentUser == null) {
            user.setRole(new User.Role());
            userRepository.save(user);
            user.setPassword(null);
            logger.info("register " + username + " successfully with role " + user.getRole());
            return new ResponseEntity<User>(user, HttpStatus.OK);
        } else {
            logger.info(username + " not register");
            return new ResponseEntity<User>(HttpStatus.UNAUTHORIZED);
        }
    }

    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    public ResponseEntity<User> logout(@RequestBody User user) {

        String username = user.getUsername();
        logger.info("logout " + username);

        return new ResponseEntity<User>(user, HttpStatus.OK);
    }
}
