package com.getbookmarks.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@RequestMapping("/ping")
@Controller
public class PingResource {

    @Autowired
    private Environment env;

    @RequestMapping(produces = MediaType.ALL_VALUE)
    @ResponseBody
    public String ping() {
        return "{'ping':'pong'}";
    }

    @RequestMapping(value = "env",produces = MediaType.ALL_VALUE)
    @ResponseBody
    public String property() {
        return "{\"env\":\""+env.getProperty("app.env")+"\"}";
    }
}
