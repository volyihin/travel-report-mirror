package com.getbookmarks.rest;

import com.getbookmarks.domain.Story;
import com.getbookmarks.domain.record.ImageRecord;
import com.getbookmarks.domain.record.Record;
import com.getbookmarks.domain.record.RecordType;
import com.getbookmarks.domain.record.TextRecord;
import com.getbookmarks.repository.RecordRepository;
import com.getbookmarks.repository.StoryRepository;
import com.google.common.base.Strings;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

/**
 * Created by volyihin on 8/29/14.
 */


@Controller
@RequestMapping(value = "/stories/{storyId}/records")
public class RecordResource {

    public static final Logger logger = Logger.getLogger(RecordResource.class);

    private RecordRepository recordRepository;
    private StoryRepository storyRepository;

    @Autowired
    @SuppressWarnings("unused")
    public RecordResource(RecordRepository recordRepository, StoryRepository storyRepository) {
        this.recordRepository = recordRepository;
        this.storyRepository = storyRepository;
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Record> getRecords(@PathVariable("storyId") String storyId) {
        logger.info("get records by storyId = " + storyId);
        return recordRepository.findByStoryId(storyId);
    }

    @RequestMapping(value = "/{recordId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Record> getRecord(@PathVariable("storyId") String storyId, @PathVariable("recordId") String recordId) {
        logger.info("get records ");
        Record record = recordRepository.findOne(recordId);

        return new ResponseEntity<Record>(record, HttpStatus.OK);
    }

    @RequestMapping(value = "/text", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Record> addTextRecord(@PathVariable("storyId") String storyId, @RequestBody TextRecord record, @RequestParam("index") String index) {
        logger.info("add text record to storyId=" + storyId + " to index=" + index);
        ResponseEntity<Record> recordResponse = createRecord(storyId, record, index);
        logger.info("record added");
        return recordResponse;
    }

    @RequestMapping(value = "/move", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Void> addTextRecord(@PathVariable("storyId") String storyId, @RequestParam("origin") String origin, @RequestParam("destination") String destination) {
        logger.info("move records to storyId=" + storyId + " from "+ origin +" to destination=" + destination);

        Story story = storyRepository.findOne(storyId);

        int originValue = Integer.valueOf(origin);
        int destValue = Integer.valueOf(destination);
        Collections.swap(story.getRecords(), originValue, destValue);
        storyRepository.save(story);
        logger.info("records moved");
        return  new ResponseEntity<Void>(HttpStatus.OK);
    }

    private ResponseEntity<Record> createRecord(String storyId, Record record, String index) {
        Story story = storyRepository.findOne(storyId);
        recordRepository.save(record);
        if (Strings.isNullOrEmpty(index)) {
            story.addRecord(record);
        } else {
            Integer indexValue = Integer.valueOf(index);
            story.addRecord(record, indexValue);
        }
        storyRepository.save(story);
        return new ResponseEntity<Record>(record, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/image", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Record> addImageRecord(@PathVariable("storyId") String storyId, @RequestBody ImageRecord record) {
        logger.info("add image record " + storyId + record);
        return createRecord(storyId, record,null);
    }

    @RequestMapping(value = "/{recordId}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<TextRecord> updateTextRecord(@PathVariable("storyId") String storyId, @RequestBody TextRecord record) {

        logger.info(String.format("update record %s %s %s", storyId, record.getId(), record.getText()));

        TextRecord savedRecord = recordRepository.save(record);

        return new ResponseEntity<TextRecord>(savedRecord, HttpStatus.OK);
    }

    @RequestMapping(value = "/{recordId}", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<Void> deleteRecord(@PathVariable("storyId") String storyId, @PathVariable("recordId") String recordId) {
        logger.info("delete record " + recordId);
        Story story = storyRepository.findOne(storyId);

        story.removeRecord(recordId);

        storyRepository.save(story);

        recordRepository.delete(recordId);

        return new ResponseEntity<Void>(HttpStatus.OK);
    }


}

