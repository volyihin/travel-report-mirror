package com.getbookmarks.domain;

import com.google.common.collect.Lists;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * Created by volyihin on 8/29/14.
 */
@Document
public class User {

    @Id
    private String id;
    private String username;
    private String password;

    @DBRef
    private List<Trip> trips = Lists.newArrayList();

    public User(String id, String username, String password, Role role, boolean rememberme) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.role = role;
        this.rememberme = rememberme;
    }

    public List<Trip> getTrips() {
        return trips;
    }

    public User(String username) {
        this.username = username;
        this.role = new Role();
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    private Role role;
    private boolean rememberme;


    public User() {
    }

    public String getUsername() {
        return username;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isRememberme() {
        return rememberme;
    }

    public void setRememberme(boolean rememberme) {
        this.rememberme = rememberme;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (rememberme != user.rememberme) return false;
        if (!id.equals(user.id)) return false;
        if (!password.equals(user.password)) return false;
        if (!role.equals(user.role)) return false;
        if (!username.equals(user.username)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + username.hashCode();
        result = 31 * result + password.hashCode();
        result = 31 * result + role.hashCode();
        result = 31 * result + (rememberme ? 1 : 0);
        return result;
    }

    public void setId(String id) {
        this.id = id;
    }

    public static class Role {
        private String title = "user";
        private int bitMask = 2;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getBitMask() {
            return bitMask;
        }

        public void setBitMask(int bitMask) {
            this.bitMask = bitMask;
        }

        @Override
        public String toString() {
            return "Role{" +
                    "title='" + title + '\'' +
                    ", bitMask=" + bitMask +
                    '}';
        }
    }

}
