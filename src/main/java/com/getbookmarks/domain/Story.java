package com.getbookmarks.domain;

import java.util.*;

import com.getbookmarks.domain.record.Image;
import com.getbookmarks.domain.record.ImageRecord;
import com.getbookmarks.domain.record.Record;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "stories")
public class Story {

    @Id
    private String id;

    private String title;

    private String text;

    private final Date submittedOn = new Date();

    @DBRef
    private User user;

    private String tripId;

    @DBRef
    private Image image;

    @DBRef
    private List<Record> records = new ArrayList<Record>();

    public Story() {
    }

    public static Story storyProxy(String id) {
        return new Story(id);
    }

    public Story(String id) {
        this.id = id;
    }

    public Story(String id, String title, String text, String fullname, User user, Image image, List<Record> records) {
        this.id = id;
        this.title = title;
        this.text = text;
        this.user = user;
        this.image = image;
        this.records = records;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }

    public Date getSubmittedOn() {
        return submittedOn;
    }

    public Image getImage() {
        return image;
    }

    public void addRecord(Record record) {
        records.add(record);
    }

    public void addRecord(Record record, int index) {
        records.add(index,record);
    }

    public void removeRecord(String recordId) {
        Iterator<Record> it = records.iterator();
        while (it.hasNext()) {
            Record record = it.next();
            if (record.getId().equals(recordId)) {
                it.remove();
            }
        }
    }

    public void addImageRecord(ImageRecord record) {
        records.add(record);
    }

    public List<Record> getRecords() {
        return this.records;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public String getTripId() {
        return this.tripId;
    }

    @Override
    public String toString() {
        return "Story{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", text='" + text + '\'' +
                ", submittedOn=" + submittedOn +
                ", image='" + image + '\'' +
                ", records=" + records +
                '}';
    }

    public void addImage(Image image) {
        this.image = image;
    }

    public User getUser() {
        return user;
    }

    public void setImage(Image image) {
        this.image = image;
    }

}
