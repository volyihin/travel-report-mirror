package com.getbookmarks.domain;

import com.getbookmarks.domain.record.Image;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nataliyamakarova on 11.12.14.
 */
@Document(collection = "trips")
public class Trip {

    @Id
    private String id;

    private String name;

    @DBRef
    private Image image;

    private String username;

    @DBRef
    private List<Story> stories = new ArrayList<Story>();

    public Trip() {

    }

    public static Trip proxyTrip(String id) {
        return new Trip(id);
    }

    public Trip(String id) {
        this.id = id;
    }

    public Trip(String id, String name, Image image, String username, List<Story> stories) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.username = username;
        this.stories = stories;
    }

    public Trip(Image image, String username) {
        this.image = image;
        this.username = username;
    }

    public String getId() {
        return id;
    }

    public List<Story> getStories() {
        return stories;
    }

    public Image getImage() {
        return image;
    }

    public String getName() {
        return name;
    }

    public String getUsername() {
        return username;
    }

    @Override
    public String toString() {
        return "Trip{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", image=" + image +
                ", username='" + username + '\'' +
                ", stories=" + stories +
                '}';
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setStories(List<Story> stories) {
        this.stories = stories;
    }
}
