package com.getbookmarks.domain.record;

import com.getbookmarks.domain.record.RecordType;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Comparator;
import java.util.Objects;
import com.getbookmarks.domain.Story;

/**
 * Created by volyihin on 8/28/14.
 */
@Document(collection = "records")
public class Record {

    @Id
    protected String id;

    protected String type;

    protected String storyId;


    public Record(String type, String storyId) {
        this.type = type;
        this.storyId = storyId;
    }

    public Record() {
    }

    public Record(String id, String type, String storyId) {
        this.id = id;
        this.type = type;
        this.storyId = storyId;
    }

    public String getId() {
        return this.id;
    }

    public String getType() {
        return type;
    }

    public String getStoryId() {
        return storyId;
    }

    @Override
    public String toString() {
        return "{" +
                "id='" + id + '\'' +
                "type='" + type + '\'' +
                "storyId='" + storyId + '\'' +
                '}';
    }

}
