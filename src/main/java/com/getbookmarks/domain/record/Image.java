package com.getbookmarks.domain.record;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by nataliyamakarova on 13.09.14.
 */
@Document(collection = "images")
public class Image {

    @Id
    private String id;
    private String url;
    private boolean vertical;

    public Image() {
    }

    public Image(String url, boolean vertical) {
        this.url = url;
        this.vertical = vertical;
    }

    public Image(String url) {
        this.url = url;
    }

    public String getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public boolean isVertical() {
        return vertical;
    }

    @Override
    public String toString() {
        return "Image{" +
                "id='" + id + '\'' +
                ", url='" + url + '\'' +
                ", vertical=" + vertical +
                '}';
    }
}
