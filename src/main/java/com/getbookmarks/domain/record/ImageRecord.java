package com.getbookmarks.domain.record;

import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.getbookmarks.domain.Story;

/**
 * Created by nataliyamakarova on 19.12.14.
 */
@Document(collection = "records")
public class ImageRecord extends Record {

    @DBRef
    private Image image;

    public ImageRecord(Image image,String storyId) {
        super(RecordType.Image.name(),storyId);
        this.image = image;
    }

    public Image getImage() {
        return image;
    }

    @Override
    public String toString() {
        return "ImageRecord{" +
                "image=" + image +
                '}';
    }
}
