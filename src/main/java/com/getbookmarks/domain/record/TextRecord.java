package com.getbookmarks.domain.record;

import org.springframework.data.mongodb.core.mapping.Document;

import com.getbookmarks.domain.Story;

/**
 * Created by nataliyamakarova on 19.12.14.
 */
@Document(collection = "records")
public class TextRecord extends Record {

    private String text;

    public TextRecord() {
    };

    public TextRecord(String text, long number,String storyId) {
        super(RecordType.Image.name(),storyId);
        this.text = text;
    }

    public String getText() {
        return text;
    }

    @Override
    public String toString() {
        return "TextRecord{" +
                "text='" + text + '\'' +
                '}';
    }
}
