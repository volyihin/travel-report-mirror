package com.getbookmarks.repository;

import com.getbookmarks.domain.Story;
import com.getbookmarks.domain.record.Record;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by volyihin on 8/29/14.
 */
@Repository
public interface RecordRepository extends CrudRepository<Record, String> {

   @Query(value = "{ '_id': 0? }, { $set: { 'comment': 1?} }")
   public Record updateComment(String recordId, String comment);

   public List<Record> findByStoryId(String storyId);
}
