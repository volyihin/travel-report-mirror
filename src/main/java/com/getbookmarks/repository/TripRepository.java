package com.getbookmarks.repository;

import com.getbookmarks.domain.record.Record;
import com.getbookmarks.domain.Story;
import com.getbookmarks.domain.Trip;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by nataliyamakarova on 12.12.14.
 */
@Repository
public interface TripRepository extends CrudRepository<Trip, String> {
//
//    @Query(value = "{ '_id': 0? }, { $set: { 'comment': 1?} }")
//    public Record updateComment(String recordId, String comment);
    public List<Trip> findAll();

    public List<Trip> findByUsername(String username);
}
