package com.getbookmarks.repository;

import com.getbookmarks.domain.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by volyihin on 8/29/14.
 */
@Repository
public interface UserRepository extends CrudRepository<User,String> {

    public User findByUsername(String username);
}
