package com.getbookmarks.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.getbookmarks.domain.Story;
import com.getbookmarks.domain.Trip;

@Repository
public interface StoryRepository extends CrudRepository<Story, String> {

    public List<Story> findAll();

    public List<Story> findByTripId(String tripId);
}
