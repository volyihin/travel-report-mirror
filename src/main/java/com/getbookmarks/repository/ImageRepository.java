package com.getbookmarks.repository;

import com.getbookmarks.domain.record.Image;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by nataliyamakarova on 13.09.14.
 */
@Repository
public interface ImageRepository  extends CrudRepository<Image,String>{

    public Image findByUrl(String url);
}
