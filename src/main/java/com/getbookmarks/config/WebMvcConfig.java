package com.getbookmarks.config;

import com.getbookmarks.domain.Story;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.data.repository.init.Jackson2ResourceReader;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.json.MappingJacksonJsonView;

import com.getbookmarks.rest.PingResource;

import java.io.IOException;

@EnableWebMvc
@ComponentScan(basePackageClasses = PingResource.class)
@Configuration
@PropertySource("classpath:app.properties")
public class WebMvcConfig extends WebMvcConfigurerAdapter {

    @Bean
    public MappingJacksonJsonView jsonView() {
        MappingJacksonJsonView jsonView = new MappingJacksonJsonView();
        jsonView.setPrefixJson(true);
        return jsonView;
    }

    @Bean
    public MultipartResolver multipartResolver() {
        return new StandardServletMultipartResolver();
    }

    public static void main(String[] args) throws IOException {


        String jsonInput = "{\"id\":\"53ff9f4f03ce96554cc5e353\",\"title\":\"1231231\",\"text\":\"123213123\",\"url\":null,\"fullname\":\"123123123\",\"submittedOn\":1409261391650,\"image\":null,\"records\":[{\"image\":\"ergerg\",\"comment\":\"ergerg\"}]}";
        ObjectMapper mapper = new ObjectMapper();
        Story story = mapper.readValue(jsonInput, Story.class);
        System.out.println(mapper.writeValueAsString(story));
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/img/**").addResourceLocations("/img/");
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }
}
