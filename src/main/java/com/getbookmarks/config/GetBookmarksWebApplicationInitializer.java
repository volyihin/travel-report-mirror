package com.getbookmarks.config;

import org.apache.log4j.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration.Dynamic;
import com.google.common.base.MoreObjects;

public class GetBookmarksWebApplicationInitializer implements WebApplicationInitializer  {

    public static final Logger logger = Logger.getLogger(GetBookmarksWebApplicationInitializer.class);
    private static String TEMP_DIR = MoreObjects.firstNonNull(System.getenv("OPENSHIFT_DATA_DIR"), "/tmp");

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        AnnotationConfigWebApplicationContext webApplicationContext = new AnnotationConfigWebApplicationContext();
        webApplicationContext.register(ApplicationConfig.class, WebMvcConfig.class, SecurityConfig.class);

        Dynamic dispatcher = servletContext.addServlet("dispatcherServlet", new DispatcherServlet(webApplicationContext));
        dispatcher.addMapping("/api/v1/*");
        dispatcher.setLoadOnStartup(1);

//        ServletRegistration servletRegistration = servletContext.getServletRegistration("default");
//        servletRegistration.addMapping("/img/*");
//        servletRegistration.addMapping("*.jpg");

        logger.info("TEMP_DIR=" + TEMP_DIR);    

        dispatcher.setMultipartConfig(
                new MultipartConfigElement(TEMP_DIR, 25 * 1024 * 1024, 125 * 1024 * 1024, 1 * 1024 * 1024)
        );

//        servletContext.addFilter("myFilter", MyFilter.class).addMappingForUrlPatterns(null, false, "/*");


//        initLogger();

    }

    private void initLogger() {

        String logFolder = System.getenv("OPENSHIFT_LOG_DIR");

        ConsoleAppender console = new ConsoleAppender(); //create appender
        //configure the appender
        String PATTERN = "%d [%p|%c|%C{1}] %m%n";
        console.setLayout(new PatternLayout(PATTERN));
        console.setThreshold(Level.ALL);
        console.activateOptions();
        //add appender to any Logger (here is root)
        Logger.getRootLogger().addAppender(console);

        FileAppender fa = new FileAppender();
        fa.setName("FileLogger");
        fa.setFile(logFolder + "app.log");
        fa.setLayout(new PatternLayout("%d %-5p [%c{1}] %m%n"));
        fa.setThreshold(Level.ALL);
        fa.setAppend(true);
        fa.activateOptions();

        //add appender to any Logger (here is root)
        Logger.getRootLogger().addAppender(fa);
        //repeat with all other desired appenders
    }


}
