package com.getbookmarks.config;

import com.getbookmarks.domain.User;
import com.getbookmarks.repository.StoryRepository;
import com.mongodb.Mongo;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.authentication.UserCredentials;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.data.mongodb.core.convert.DefaultMongoTypeMapper;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;

import static com.google.common.base.Strings.isNullOrEmpty;

@Configuration
@EnableMongoRepositories(basePackageClasses = StoryRepository.class)
public class ApplicationConfig {

    private static final Logger logger = Logger.getLogger(ApplicationConfig.class);

    private static final String DEFAULT_MONGO_HOST = "localhost";
    private static final int DEFAULT_MONGO_PORT = 27017;
    private static final String MONGO_USERNAME = "admin";
    private static final String MONGO_PASSWORD = "admin";
    private static final String DEFAULT_MONGO_DB_NAME = "travel";

    @Bean
    public MongoTemplate mongoTemplate() throws Exception {

        String mongoDbHost = getDefaultDbHost();
        Integer mongoDbPort = getDefaultDbPort();

        Mongo mongo = new Mongo(mongoDbHost, mongoDbPort);

        String username = getDefaultUsername();
        String password = getDefaultPassword();

        UserCredentials userCredentials = new UserCredentials(username, password);

        String databaseName = getDefaultDbName();
        MongoDbFactory mongoDbFactory = new SimpleMongoDbFactory(mongo, databaseName, userCredentials);

        // //remove _class
        // MappingMongoConverter converter = new MappingMongoConverter(mongoDbFactory, new MongoMappingContext());
        // converter.setTypeMapper(new DefaultMongoTypeMapper(null));

        MongoTemplate mongoTemplate = new MongoTemplate(mongoDbFactory);

        insertAdminUser(mongoTemplate);

        insertUser(mongoTemplate);

        return mongoTemplate;
    }

    private void insertAdminUser(MongoTemplate mongoTemplate) {
        User adminUser = new User("admin");
        User.Role adminRole = new User.Role();
        adminRole.setBitMask(4);
        adminRole.setTitle("admin");

        adminUser.setRole(adminRole);
        Query existAdminQuery = new Query();
        existAdminQuery.addCriteria(Criteria.where("username").regex("admin"));
        boolean adminExist = mongoTemplate.exists(existAdminQuery, User.class);
        if (!adminExist) {
            logger.info("admin user was created");
            mongoTemplate.insert(adminUser);
        }
    }

    private void insertUser(MongoTemplate mongoTemplate) {
        User user = new User("user");
        Query existAdminQuery = new Query();
        existAdminQuery.addCriteria(Criteria.where("username").regex("user"));
        boolean adminExist = mongoTemplate.exists(existAdminQuery, User.class);
        if (!adminExist) {
            logger.info("basic user was created");
            mongoTemplate.insert(user);
        }
    }

    private String getDefaultDbName() {
        String databaseName = System.getenv("OPENSHIFT_APP_NAME");

        if (isNullOrEmpty(databaseName)) {
            return DEFAULT_MONGO_DB_NAME;
        } else {
            return databaseName;
        }
    }

    private String getDefaultDbHost() {
        String mongoDbHost = System.getenv("OPENSHIFT_MONGODB_DB_HOST");

        if (isNullOrEmpty(mongoDbHost)) {
            return DEFAULT_MONGO_HOST;
        } else {
            return mongoDbHost;
        }
    }

    private Integer getDefaultDbPort() {
        String mongoDbPort = System.getenv("OPENSHIFT_MONGODB_DB_PORT");

        if (isNullOrEmpty(mongoDbPort)) {
            return DEFAULT_MONGO_PORT;
        } else {
            try {
                return Integer.parseInt(mongoDbPort);
            } catch (NumberFormatException nfe) {
                return DEFAULT_MONGO_PORT;
            }
        }
    }

    private String getDefaultUsername() {

        String username = System.getenv("OPENSHIFT_MONGODB_DB_USERNAME");

        if (isNullOrEmpty(username)) {
            return MONGO_USERNAME;
        } else {
            return username;
        }

    }

    private String getDefaultPassword() {
        String password = System.getenv("OPENSHIFT_MONGODB_DB_PASSWORD");

        if (isNullOrEmpty(password)) {
            return MONGO_PASSWORD;
        } else {
            return password;
        }

    }


}
